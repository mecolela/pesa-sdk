'use strict';

const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const HtmlMinifyOpts = {
  collapseWhitespace: true,
  removeComments: true,
  removeRedundantAttributes: true,
  removeScriptTypeAttributes: true,
  removeStyleLinkTypeAttributes: true,
  useShortDoctype: true
};

module.exports = {
  mode: 'production',
  entry: {
    'modal.framer': './src/modal',
    'inline.framer': './src/inline',
    'modal.controller': './src/modal/controller',
    'inline.controller': './src/inline/controller',
    'modal.frame': ['./src/modal/frame', './src/modal/frame/styles.css'],
    'inline.frame': ['./src/inline/frame', './src/inline/frame/styles.css']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  optimization: {
    minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({fileName: '[name].css'}),
    new HtmlWebpackPlugin({
      meta: {
        viewport: 'width=device-width,initial-scale=1'
      },
      minify: HtmlMinifyOpts,
      chunks: ['inline.frame'],
      title: 'Pesa Inline',
      template: './src/inline/frame/index.html',
      inlineSource: '.(js|css)$',
      filename: 'inline.frame.html'
    }),
    new HtmlWebpackPlugin({
      meta: {
        viewport: 'width=device-width,initial-scale=1'
      },
      minify: HtmlMinifyOpts,
      chunks: ['modal.frame'],
      title: 'Pesa Modal',
      template: './src/modal/frame/index.html',
      inlineSource: '.(js|css)$',
      filename: 'modal.frame.html'
    }),
    new HtmlWebpackPlugin({
      chunks: ['modal.controller'],
      title: 'Ctl',
      minify: HtmlMinifyOpts,
      inlineSource: '.js$',
      template: './src/index.controller.template.html',
      filename: 'modal.controller.html'
    }),
    new HtmlWebpackPlugin({
      chunks: ['inline.controller'],
      title: 'Ctl',
      minify: HtmlMinifyOpts,
      inlineSource: '.js$',
      template: './src/index.controller.template.html',
      filename: 'inline.controller.html'
    }),
    new HtmlWebpackInlineSourcePlugin()
  ]
};
