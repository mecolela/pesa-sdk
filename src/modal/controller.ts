const modal = {
  async submit(frameId: string) {
    const data = await this.getFormData(frameId);
    if (data) {
      return fetch('http://localhost:3000', {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }).then(res => res.json());
    } else return false;
  },
  getFormData(id: string) {
    return new Promise((resolve => {
      // @ts-ignore
      const frame = parent[`${id}_frm`];

      const doc = frame.document;
      const num = doc.getElementById('c-num').value,
        cvc = doc.getElementById('c-cvc').value,
        exp = doc.getElementById('c-exp').value;

      if (!num) {
        // TODO: concat errors here
      }
      if (!cvc) {
        // TODO: concat errors here
      }
      if (!exp) {
        // TODO: concat errors here
      }

      frame.postMessage({action: 'scoop'});

      function handler(ev: any) {
        const data = ev.data;
        if (data.action === `scoop`) {
          resolve({id, ...data.form});
          window.removeEventListener('message', handler);
        }
      }

      window.addEventListener('message', handler);
    }));
  }
};

window.addEventListener('message', async (ev: MessageEvent) => {
  let parentUrl: string | Location;
  if (parent) {
    parentUrl = (window.location !== window.parent.location) ? document.referrer : document.location;
  }

  const data = ev.data;
  if (data.action === 'submit') {
    const form = data.frames[0];
    const val = await modal.submit(form.id);
    if (val) {
      // @ts-ignore
      ev.source.postMessage({
        // TODO: support error handling
        ok: true,
        id: form.id,
        action: 'response',
        result: val
      }, ev.origin);
    }
  } else if (data.action === 'error') {
    if (typeof parentUrl === 'string') {
      parent.window.postMessage({
        type: 'error', field: data.field,
        message: data.message
      }, parentUrl);
    }
  }
});
