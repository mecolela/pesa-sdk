import { attachToElement, createIframe, generateId, styler } from '../utils';

interface IPayRequest {
  customer: {
    firstName: string
    lastName: string
    email: string
    phone?: string
  };
  amount: number;
  styles?: string;
  country?: string;
  currency?: string;
}

function Pesa(key: string) {
  if (!(this instanceof Pesa)) {
    // @ts-ignore
    return new Pesa(key);
  }

  this._init(key);
}

Pesa.prototype._init = async function (key: string) {
  // Verify API key with the server or something
};

Pesa.prototype.requestPay = async function (opts: IPayRequest) {
  const options = Object.assign({
    customer: {},
    amount: 1,
    styles: null,
    country: 'KE',
    currency: 'KES'
  }, opts);

  const id = generateId();

  const frame = createIframe({
    id, type: 'MODAL_FRAME',
    hash: styler.hash(options.styles)
  });

  attachToElement('body.append', frame);

  return this._awaitResult(id);
};

Pesa.prototype._awaitResult = function (frameId: string) {
  return new Promise((res, rej) => {
    console.log(window.frames[frameId]);
    window.frames[frameId].addEventListener('message', (ev: any) => {
      const data = ev.data;
      if (data.action === 'final') {
        data.ok ? res(data.result) : rej(data.result);
      }
    });
  });
};

declare const window: any;
if (typeof window !== 'undefined' && typeof window.Pesa === 'undefined') {
  window.Pesa = Pesa;
}
