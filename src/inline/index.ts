import { attachToElement, createIframe, generateId, ObjectType, styler } from '../utils';

const serverUrl = 'http://localhost:3000';

function Pesa(key: string) {
  if (!(this instanceof Pesa)) {
    // @ts-ignore
    return new Pesa(key);
  }

  this._init(key);
  this.frames = [];
  this.isRegistered = false;
}

Pesa.prototype._init = async function (key: string) {
  // Verify API key with the server or something
};

Pesa.prototype.elements = function () {
  const id = generateId();

  return {
    create(type: string | string[], options: {styles: any} = {styles: null}) {
      // TODO: support more options later : cardNumber, cardCvc, cardExpiry
      const types = ['card'];
      if (Array.isArray(type)) {
        type.forEach(t => {
          if (types.indexOf(t) === -1) {
            throw `Invalid field type: ${t}`;
          }
        });
      } else {
        if (types.indexOf(type) === -1) {
          throw `Invalid field type: ${type}`;
        }
      }

      const stylesHash = styler.hash(options.styles);

      function _listener(func: (ev: any) => void) {
        return (message: any) => {
          const data = message.data;
          if (ObjectType(data) === '[object Object]' && data.type) {
            func(data);
          }
        };
      }

      return {
        id, type,
        addEventListener(func: (ev: any) => void) {
          if (typeof func !== 'undefined') {
            window.addEventListener('message', _listener(func));
          }
        },
        removeEventListener(func: (ev: any) => void) {
          if (typeof func !== 'undefined') {
            window.removeEventListener('message', _listener(func));
          }
        },
        mount(selector: string) {
          document.addEventListener('DOMContentLoaded', () => {
            const form = createIframe({
              type: 'INLINE_FRAME',
              // TODO: review on supporting multiple input types
              inputType: Array.isArray(type) ? type[0] : type,
              id: `${id}_frm`,
              hash: stylesHash
            });
            if (form) {
              attachToElement(selector, form);
            } else {
              throw 'An element with the same ID already exists';
            }
          });
        }
      };
    }
  };
};

Pesa.prototype.register = function (frames: any | any[]): any {
  if (this.isRegistered) {
    return null;
  }
  if (Array.isArray(frames)) {
    this.frames = frames.map(frame => ({id: frame.id, type: frame.type}));
  } else {
    const exists = this.frames.find((x: any) => x.id === frames.id);
    if (!exists) {
      this.frames.push({id: frames.id, type: frames.type});
    }
  }
  document.addEventListener('DOMContentLoaded', () => {
    const ctl = createIframe({
      type: 'INLINE_CONTROLLER',
      id: '__pesa_ctl'
    });
    if (ctl) attachToElement('body.append', ctl);
  });
};

Pesa.prototype.submit = function (element: string) {
  return new Promise((resolve, reject) => {
    // @ts-ignore
    window.frames['__pesa_ctl'].postMessage({
      action: 'submit',
      frames: this.frames
    }, serverUrl);

    const handler = (ev: any) => {
      const data = ev.data;
      if (data.action === 'response') {
        data.ok ? resolve(data.result) : reject(data.result);
      }
      window.removeEventListener('message', handler);
    };

    window.addEventListener('message', handler);
  });
};

if (typeof window !== 'undefined' && typeof (window as any).Pesa === 'undefined') {
  (window as any).Pesa = Pesa;
}
