import { styler } from '../../utils';

let form: any;

const luhnCheck = (function (arr) {
  return function (ccNum: string) {
    let len = ccNum.length,
      bit = 1,
      sum = 0,
      val;

    while (len) {
      val = parseInt(ccNum.charAt(--len), 10);
      sum += (bit ^= 1) ? arr[val] : val;
    }

    return sum && sum % 10 === 0;
  };
}([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]));

function focus(element: string, setCursorEnd: boolean = false) {
  const el = document.getElementById(`c-${element}`);
  el.focus();
  if (setCursorEnd) {
    // TODO: Fix aligning issues when navigating with arrow keys
    // console.log('len', el.value.length);
    // console.log('start', el.selectionStart);
    // console.log('end', el.selectionEnd);
    // el.setSelectionRange(20, 20);
  }
}

function format(type: string, target: any) {
  const value = target.value;
  if (type === 'num') {
    const v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, ''),
      matches = v.match(/\d{4,16}/g),
      match = matches && matches[0] || '',
      parts = [];

    for (let i = 0, len = match.length; i < len; i += 4) {
      parts.push(match.substring(i, i + 4));
    }

    if (parts.length) {
      target.value = parts.join(' ');
    } else {
      target.value = value;
    }

    if (target.value.length === 19) {
      let valid = luhnCheck(target.value.replace(/ /g, ''));
      if (valid) {
        focus('exp');
      } else {
        bubbleError({field: 'number', message: 'Your card number is invalid.'});
      }
    }
  } else if (type === 'exp') {
    let sanitized = value.replace(
      /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
    ).replace(
      /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
    ).replace(
      /^1([3-9])$/g, '01/$1' // 13 > 01/3
    ).replace(
      /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
    ).replace(
      /^0\/|0+$/g, '0' // 0/ > 0 and 00 > 0
    ).replace(
      /[^\d|^\/]*/g, '' // To allow only digits and `/`
    ).replace(
      /\/\//g, '/' // Prevent entering more than 1 `/`
    );

    if (sanitized.length >= 3 && sanitized.indexOf('/') === -1) {
      let temp = [...sanitized];
      temp.splice(2, 0, '/');
      sanitized = temp.join('');
    }

    const parts = sanitized.split('/');
    const month = parseInt(parts[0], 10);
    const year = parseInt(parts[1], 10) || null;
    const currentYear = parseInt(new Date().getFullYear().toString().substr(-2), 10);
    const currentMonth = parseInt(('0' + (new Date().getMonth() + 1)).slice(-2), 10);

    const result = parts.join(' / ');
    if (result.length < 7) {
      target.value = result;
    } else {
      target.value = result.substr(0, 7);
    }

    if (year && !isNaN(year)) {
      if (result.length > 1 && (year === currentYear && month < currentMonth)) {
        return bubbleError({field: 'exp-year', message: 'Your cards expiration month is invalid.'});
      }

      if (result.length === 7 && (year < currentYear || year > currentYear + 50)) {
        return bubbleError({field: 'exp-month', message: 'Your cards expiration year is invalid.'});
      }

      if (`${year}`.length === 2 && month) {
        focus('cvc');
      }
    }
  } else if (type === 'cvc') {
    target.value = value.replace(/[^0-9]/gi, '').substr(0, 4);
  }
}

function handleKeys(type: string, ev: any) {
  const {key, target} = ev;
  const length = target.value.length;
  const position = target.selectionStart;
  if (type === 'num') {
    if (key === 'ArrowRight') {
      if ((position === 19 && length === 19) || (!position && !length)) {
        focus('exp');
      }
    }
  } else if (type === 'exp') {
    switch (key) {
      case 'ArrowLeft':
        if (!position) {
          focus('num', true);
        }
        break;
      case 'ArrowRight':
        if ((position === 7 && length === 7) || (!position && !length)) {
          focus('cvc');
        }
        break;
    }
  } else if (type === 'cvc') {
    if (key === 'ArrowLeft' && !position) {
      focus('exp', true);
    }
  }
}

function getFormData() {
  const f = {
    number: form.num.value,
    cvc: parseInt(form.cvc.value),
    expMonth: parseInt(form.exp.value.substr(0, 2), 10),
    expYear: parseInt(form.exp.value.substr(-2), 10)
  };

  // TODO: perform a cumulative issue check & bubble accordingly from here, not the controller

  if (
    f.number
    && f.cvc
    && f.expMonth
    && f.expYear
  ) {
    return f;
  }
  return false;
}

function bubbleError(message: any) {
  // TODO: Handle assumption that more than 1 form exists;
  // const id = window.location.pathname.split('/')[2];
  // @ts-ignore
  window.parent.frames['__pesa_ctl'].postMessage({action: 'error', ...message}, window.location.origin);
}

window.onload = () => {
  if (window.location) {
    styler.decode(window.location.hash);
  }

  form = {
    self: document.getElementsByTagName('form')[0],
    type: document.getElementById('c-type'),
    num: document.getElementById('c-num'),
    exp: document.getElementById('c-exp'),
    cvc: document.getElementById('c-cvc')
  };

  form.self.onsubmit = (ev: Event) => ev.preventDefault();
  form.type.onclick = () => focus('num');
  form.num.oninput = (ev: Event) => format('num', ev.target);
  form.exp.oninput = (ev: Event) => format('exp', ev.target);
  form.cvc.oninput = (ev: Event) => format('cvc', ev.target);

  form.num.onkeydown = (ev: KeyboardEvent) => handleKeys('num', ev);
  form.exp.onkeydown = (ev: KeyboardEvent) => handleKeys('exp', ev);
  form.cvc.onkeydown = (ev: KeyboardEvent) => handleKeys('cvc', ev);

  window.addEventListener('message', (ev) => {
    const data = ev.data;
    if (!data.action) return;

    switch (data.action) {
      case 'scoop':
        // @ts-ignore
        ev.source.postMessage({action: 'scoop', form: getFormData()});
        break;
      case 'focus':
        focus('num');
        break;
    }
  });
};
