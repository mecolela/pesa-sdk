import { flatten } from 'flat';

export function generateId(): string {
  // TODO: genrate an id that profiles the current environment / user
  return 'xxxxxxxxxxxxxxxxxx'.replace(/[x]/g, () => ((Math.random() * 16) | 0).toString(16));
}

export const ObjectType = (obj: any) => Object.prototype.toString.call(obj);

interface ICreateIframeOptions {
  id: string;
  type: 'MODAL_FRAME' | 'MODAL_CONTROLLER' | 'INLINE_FRAME' | 'INLINE_CONTROLLER';
  inputType?: string;
  hash?: string;
}

export function createIframe(options: ICreateIframeOptions, baseUrl: string = 'http://localhost:3000'): Element | false {

  const exists = document.getElementById(options.id);

  if (exists) {
    return false;
  }

  const buildUrl = function (selector: string, inputType?: string, hash?: string) {
    return `${baseUrl}/${selector}${hash ? '#' + hash : ''}`;
    // return `${baseUrl}/${selector}${inputType ? '/' + inputType : ''}${hash ? '/' + hash : ''}`;
  };

  const loadPicker = function (type: string, opts: any) {
    switch (type) {
      case 'INLINE_FRAME':
        return buildUrl('inl/frm', opts.inputType ? opts.inputType : null, opts.hash ? opts.hash : null);
      case 'MODAL_FRAME':
        return buildUrl('mdl/frm', null, opts.hash ? opts.hash : null);
      case 'INLINE_CONTROLLER':
        return buildUrl('inl/ctl');
      case 'MODAL_CONTROLLER':
        return buildUrl('mdl/ctl');
    }
  };

  const node = document.createElement('div');
  node.setAttribute('class', `__pesaFrame${options.id}`);
  node.setAttribute('style', 'position: relative !important;opacity: 1 !important;display: block !important;border: none !important;margin: 0 !important;padding: 0 !important;');

  // TODO: check the current window & ensure it is SSL secured
  const frame = document.createElement('iframe');
  frame.setAttribute('frameborder', '0');
  frame.setAttribute('allowTransparency', 'true');
  frame.setAttribute('scrolling', 'no');
  frame.setAttribute('name', options.id);
  frame.setAttribute('allowpaymentrequest', 'true');
  frame.src = loadPicker(options.type, options);

  if (options.type === 'INLINE_CONTROLLER') {
    frame.setAttribute('style', 'display: none !important;');
    return frame;
  } else if (options.type === 'INLINE_FRAME') {
    frame.setAttribute('style', 'display: block !important;border: none !important;margin: 0 !important;padding: 0 !important;width: 1px !important;min-width: 100% !important;overflow: hidden !important; user-select: none !important;height: 19.2px;');
    node.appendChild(frame);
    return node;
  } else if (options.type === 'MODAL_FRAME') {
    frame.setAttribute('style', 'z-index: 2123123123; display: block !important;position: fixed !important; top: 0 !important; left: 0 !important; border: none !important;margin: 0 !important;padding: 0 !important;width: 100% !important;min-width: 100% !important;overflow: hidden !important; user-select: none !important;height: 100% !important;');
    node.appendChild(frame);
    return node;
  }
}

export function attachToElement(selector: string, form: any, serverUrl: string = 'http://localhost:3000') {
  if (selector === 'body.append') {
    document.body.appendChild(form);
  } else {
    const element: HTMLElement = document.querySelector(selector);
    element.appendChild(form);
    element.onclick = () => {
      window.frames[form.childNodes[0].name].postMessage({action: 'focus'}, serverUrl);
    };
    element.onfocus = () => {
      window.frames[form.childNodes[0].name].postMessage({action: 'focus'}, serverUrl);
    };
  }
}

export const styler = {
  hash(styles: any): string {
    if (styles) {
      let array: string[] = [];
      const flat: any = flatten(styles);
      Object.keys(flat).forEach(key => {
        let newKey = '';
        const splitKey = key.split('.');
        splitKey.forEach((k, i) => {
          if (i === 0) {
            newKey += k;
          } else {
            newKey += `[${k}]`;
          }
        });
        array.push(`${newKey}=${flat[key]}`);
      });
      return array.join('&');
    }
    return '';
  },
  decode(hash: string): any {

  }
};
