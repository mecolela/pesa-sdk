import express, { Request, Response } from 'express';
import * as bodyParser from 'body-parser';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

/*
* INLINE STUFF
* */

app.get('/inl', (req: Request, res: Response) => {
  res.sendFile('dist/inline.framer.js', {root: __dirname});
});

app.get('/inl/frm/:id?', (req: Request, res: Response) => {
  res.sendFile('dist/inline.frame.html', {root: __dirname});
});

app.get('/inl/ctl/:id?', (req: Request, res: Response) => {
  res.sendFile('dist/inline.controller.html', {root: __dirname});
});

/*
* MODAL STUFF
* */

app.get('/mdl', (req: Request, res: Response) => {
  res.sendFile('dist/modal.framer.js', {root: __dirname});
});

app.get('/mdl/frm/:id?', (req: Request, res: Response) => {
  res.sendFile('dist/modal.frame.html', {root: __dirname});
});

app.get('/mdl/ctl/:id?', (req: Request, res: Response) => {
  res.sendFile('dist/modal.controller.html', {root: __dirname});
});

/*
* DATA SUBMISSION HANDLER
* */

app.post('/', (req: Request, res: Response) => {
  const body = req.body;
  console.log('Received Card:', `...${body.number.toString().substr(-4)}`);
  res.json({
    id: body.id,
    card: {
      country: 'KE',
      // name: body.name,
      funding: 'credit',
      expYear: parseInt(body.expYear, 10),
      expMonth: parseInt(body.expMonth, 10),
      last4: body.number.toString().substr(-4)
    }
  });
});

function getIPAddress(): string {
  const interfaces = require('os').networkInterfaces();
  for (let devName in interfaces) {
    const iface = interfaces[devName];

    for (let i = 0; i < iface.length; i++) {
      const alias = iface[i];
      if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal)
        return alias.address;
    }
  }

  return '0.0.0.0';
}

app.listen(3000, '0.0.0.0', () => console.log(`Backend Server started: http://${getIPAddress()}:3000\n`));
