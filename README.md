## Pesa SDK

an attempt at improving how credit card data is submitted

### getting started

install dependencies
```
yarn install
```

start the dev watcher
```
yarn watch
```

run the mock client and server
```
yarn start
```

**The client web page will be opened in your default browser**

### next steps

- open the developer console window
- click the submit button
- expect the data from the forms iframe to be submitted to the server
which responds accordingly if successful
- check your servers console for the forms contents
